<?php

namespace Brunoocto\Json\Tests;

use Orchestra\Testbench\TestCase as TestbenchCase;
use Brunoocto\Json\Providers\JsonServiceProvider;
use Brunoocto\ExceptionHandler\Providers\ExceptionHandlerServiceProvider;
use Brunoocto\Exception\Providers\ExceptionServiceProvider;

/**
 * TestCase for Service Provider
 */
class TestCase extends TestbenchCase
{
    /**
     * Bootstrap the application
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        
        $this->artisan(
            'migrate',
            [
                '--database' => 'brunoocto_json_test',
                '--path' => realpath(__DIR__.'/database/migrations'),
                '--realpath' => realpath(__DIR__.'/database/migrations'),
            ]
        );
    }

    /**
     * Set Environement
     *
     * @return void
     */
    protected function getEnvironmentSetUp($app)
    {
        parent::getEnvironmentSetUp($app);

        $app['config']->set('database.default', 'brunoocto_json_test');
        $app['config']->set(
            'database.connections.brunoocto_json_test',
            [
                'driver'   => 'sqlite',
                'database' => ':memory:',
                'prefix'   => ''
            ]
        );
    }

    /**
     * Get package providers.
     *
     * @param  Illuminate\Foundation\Application  $app
     * @return array
     */
    protected function getPackageProviders($app)
    {
        return [
            JsonServiceProvider::class,
            ExceptionHandlerServiceProvider::class,
            ExceptionServiceProvider::class,
        ];
    }

    /**
     * Initialize Aliases
     * @param mixed $app
     * @return array
     */
    protected function getPackageAliases($app)
    {
        return [
            'LinckoJson' => 'Brunoocto\Json\Facades\JsonFacade',
        ];
    }
}
