<?php

namespace Brunoocto\Json\Tests\Feature;

use Brunoocto\Json\Tests\TestCase;
use Brunoocto\Json\Contracts\JsonInterface;

class JsonFeatureTest extends TestCase
{
    /**
     * Test some success responses
     *
     * @return void
     */
    public function testSuccessResponse()
    {
        // Build a Route only for test
        \Route::get('/tests/feature/test_success_response', function (JsonInterface $json) {
            return $json->send('test');
        });

        $response = $this->json(
            'GET',
            '/tests/feature/test_success_response'
        );

        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/vnd.api+json');
        $response->assertJsonStructure([
            'data',
            'meta' => [
                'message',
            ],
        ]);
        $response->assertJson([
            'meta' => [
                'message' => 'test',
            ],
        ]);

        // Check with more parameters
        \Route::post('/tests/feature/test_success_response', function (JsonInterface $json) {
            return $json->send('abc', 201, 'something else', 123);
        });

        $response = $this->json(
            'POST',
            '/tests/feature/test_success_response',
            ['text' => 'Some text'],
            ['Content-Type' => 'application/json',]
        );

        $response->assertJson([
            'data' => [
                'abc',
            ],
            'meta' => [
                'message' => 'something else',
            ],
        ]);
        $response->assertStatus(201);
    }

    /**
     * Test some success responses
     *
     * @return void
     */
    public function testErrorResponse()
    {
        // Check simple error
        \Route::get('/tests/feature/test_error_response', function (JsonInterface $json) {
            return $json->error();
        });

        $response = $this->json(
            'GET',
            '/tests/feature/test_error_response'
        );

        $response->assertStatus(400);
        $response->assertHeader('Content-Type', 'application/vnd.api+json');

        
        
        $response->assertJsonStructure([
            'errors' => [[
                'id',
                'status',
                'code',
                'title',
            ]],
        ]);

        $response->assertJson([
            'errors' => [[
                'code' => 1000,
            ]],
        ]);

        // Overwrite some parameters
        $_ENV['APP_NAME'] = $_SERVER['APP_NAME'] = 'Lincko Json Unit Test';
        putenv('APP_NAME="Lincko Json Unit Test"');
        \Route::patch('/tests/feature/test_error_response', function (JsonInterface $json) {
            return $json->error(1001, 'something else', 123);
        });

        $response = $this->json(
            'PATCH',
            '/tests/feature/test_error_response',
            [],
            ['Content-Type' => 'application/json',]
        );

        $response->assertStatus(401);
        $response->assertJson([
            'errors' => [[
                'meta' => [
                    'message' => 'something else',
                ],
            ]],
        ]);

        // Check non-existing code
        \Route::get('/tests/feature/test_error_response', function (JsonInterface $json) {
            return $json->error(-1);
        });

        $response = $this->json(
            'GET',
            '/tests/feature/test_error_response'
        );

        $response->assertStatus(400);
        $response->assertJson([
            'errors' => [[
                'code' => -1,
            ]],
        ]);

        // Check if we display more information in debug mode
        $app_debug = env('APP_DEBUG');
        $_ENV['APP_DEBUG'] = $_SERVER['APP_DEBUG'] = true;
        putenv('APP_DEBUG=true');
        \Route::get('/tests/feature/test_error_response', function (JsonInterface $json) {
            return $json->error();
        });

        $response = $this->json(
            'GET',
            '/tests/feature/test_error_response'
        );
        $response->assertStatus(400);
        $response->assertHeader('Content-Type', 'application/vnd.api+json');
        $response->assertJsonStructure([
            'errors' => [[
                'status',
                'code',
                'title',
                'meta' => [
                    'app',
                    'server',
                    'client',
                ],
            ]],
        ]);

        // Check if we display more information in debug mode with a message and additoinal meta data
        \Route::get('/tests/feature/test_error_response', function (JsonInterface $json) {
            return $json->error(1000, 'test', 123);
        });

        $response = $this->json(
            'GET',
            '/tests/feature/test_error_response'
        );
        $response->assertStatus(400);
        $response->assertHeader('Content-Type', 'application/vnd.api+json');
        $response->assertJsonStructure([
            'errors' => [[
                'status',
                'code',
                'title',
                'meta' => [
                    'message',
                    'app',
                    'server',
                    'client',
                ],
            ]],
        ]);

        // Return to initial setting
        $_ENV['APP_DEBUG'] = $_SERVER['APP_DEBUG'] = $app_debug;
        putenv('APP_DEBUG='.$app_debug);
        // Check That we can use HTTP status code
        \Route::get('/tests/feature/test_error_response', function (JsonInterface $json) {
            return $json->error(404);
        });

        $response = $this->json(
            'GET',
            '/tests/feature/test_error_response'
        );

        $response->assertStatus(404);
    }
}
