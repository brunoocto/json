<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBrunooctoJsonTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('brunoocto_json_test')->create('tasks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps(3);
            $table->string('title', 1000);
            $table->text('content')->nullable();
        });
    }

    /**
     * Initialize environment
     *
     * @param mixed $app
     * @return void
     */
    public function down()
    {
        // Schema::connection('brunoocto_json_test')->dropIfExists('tasks');
    }
}
