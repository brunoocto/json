<?php

namespace Brunoocto\Json\Tests\Unit\Services;

use Brunoocto\Json\Tests\TestCase;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Database\Eloquent\Collection;

class Tasks extends Model
{
    use RefreshDatabase;

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'brunoocto_json_test';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tasks';
}

class JsonServiceTest extends TestCase
{
    /**
     * Test Wait response
     *
     * @return void
     */
    public function testWait()
    {
        // Set Waiting status
        \LinckoJson::wait();

        // Success test
        $response = \LinckoJson::send();
        $this->assertEquals(200, $response->status());

        // Error test
        $response = \LinckoJson::error();
        $this->assertEquals(400, $response->status());
    }

    /**
     * Test Immediate response
     *
     * @return void
     */
    public function testImmediate()
    {
        // Success test
        $_ENV['LINCKO_JSON_FORCE_IMMEDIATE'] = $_SERVER['LINCKO_JSON_FORCE_IMMEDIATE'] = true;
        putenv('LINCKO_JSON_FORCE_IMMEDIATE=true');

        // Set Waiting status
        \LinckoJson::immediate();

        // Success test
        $response = \LinckoJson::send();
        $this->assertEquals(200, $response->status());

        // Error test
        $response = \LinckoJson::error();
        $this->assertEquals(400, $response->status());
    }

    /**
     * Test response with model
     *
     * @return void
     */
    public function testModel()
    {
        $task_1 = new Tasks;
        $task_1->title = 'A title';
        $task_1->save();

        $task_2 = new Tasks;
        $task_2->title = 'Another title';
        $task_2->title = 'Another content';
        $task_2->save();

        // Test with one item
        $single = Tasks::first();
        $response = \LinckoJson::send($single);

        $this->assertEquals(200, $response->status());

        // Check array to see if only one item is present
        $data = json_decode($response->getContent(), true);
        $this->assertEquals($data['data']['type'], 'tasks');

        $collection = Tasks::all();
        $response = \LinckoJson::send($collection);
        
        // Check array to see if only one item is present
        $data = json_decode($response->getContent(), true);
        $this->assertEquals($data['data'][0]['type'], 'tasks');
        $this->assertEquals($data['data'][1]['type'], 'tasks');
    }

    /**
     * Test error response
     *
     * @return void
     */
    public function testError()
    {
        // Error test with array in message
        $message = 'Bruno Martin';
        $response = \LinckoJson::error(402, $message);
        $this->assertEquals(402, $response->status());
        $data = json_decode($response->getContent(), true);
        // Check that the mesage is returned
        $this->assertEquals($data['errors'][0]['meta']['message'], $message);
    }

    /**
     * Test concate errors
     *
     * @return void
     */
    public function testConcatenateErrors()
    {
        // Error test with array in message
        $response = \LinckoJson::error(401, 'error01');
        $this->assertEquals(401, $response->status());

        $error_1 = json_decode($response->getContent(), true)['errors'];
        $response = \LinckoJson::error(402, 'error02', ['testing'], $error_1);
        $this->assertEquals(402, $response->status());
        
        // Check the multi error structure
        $data = json_decode($response->getContent(), true);
        // The order should be from the newest to the latest
        $this->assertEquals($data['errors'][0]['status'], 402);
        $this->assertEquals($data['errors'][1]['status'], 401);
    }
}
