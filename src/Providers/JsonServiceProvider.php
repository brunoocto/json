<?php

namespace Brunoocto\Json\Providers;

use Exception;
use Dotenv\Dotenv;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Validation\ValidationException as IlluminateValidationException;
use Illuminate\Support\ServiceProvider;
use CloudCreativity\LaravelJsonApi\Exceptions\ValidationException;
use Neomerx\JsonApi\Exceptions\ErrorCollection;
use GuzzleHttp\Exception\RequestException;

/*
 * This importation specify the real service used.
 */
use Brunoocto\Json\Services\JsonService;
use Brunoocto\Json\Contracts\JsonInterface;

class JsonServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Load environment variables specific to the library (default is .env)
        $dotenv = Dotenv::createMutable(__DIR__.'/../../');
        $dotenv->load();
        
        // Add SQLite database by merging configuration file
        $this->mergeConfigFrom(
            __DIR__.'/../../config/json-api-brunoocto-json.php',
            'json-api-brunoocto-json'
        );

        // Binding
        // The alias make sure that the Facade and the Maker will work
        $this->app->alias(JsonInterface::class, 'lincko_json');
        // The singleton (or bind) set any specification at instanciation
        $this->app->singleton(JsonInterface::class, JsonService::class);
        $this->app->singleton(JsonService::class, function () {
            return new JsonService;
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslations();

        // Because of orchestral (https://github.com/orchestral/testbench/issues/197), Exception cannot be overwritten by cerbero
        if (env('APP_ENV') != 'testing') {
            // Catch all exceptions and return them into a json format
            $this->app->make(ExceptionHandler::class)->renderer(function (Exception $exception, $request) {
                if ($this->app->isLocal() || env('APP_DEBUG')) {
                    $traces = [get_class($exception).': '.$exception->getFile(). ' ['.$exception->getLine().']'];
                    foreach ($exception->getTrace() as $trace) {
                        if (
                            array_key_exists('file', $trace)
                            && array_key_exists('line', $trace)
                            && array_key_exists('class', $trace)
                            && array_key_exists('type', $trace)
                            && array_key_exists('function', $trace)
                        ) {
                            $traces[] = $trace['file'].' ['.$trace['line'].'] '.$trace['class'].' '.$trace['type'].' '.$trace['function'];
                        } else {
                            $traces[] = json_encode($trace, JSON_UNESCAPED_UNICODE | JSON_FORCE_OBJECT);
                        }
                    }
                    // Get exception message
                    $message = $exception->getMessage();
                    // At null, $previous will be ignored in \LinckoJson::error
                    $previous = null;
                    // For Guzzle communication, if the response is an error, we concate to the current error message
                    if ($exception instanceof RequestException && $exception->hasResponse()) {
                        // Convert the content into an array
                        $content = json_decode($exception->getResponse()->getBody()->getContents(), true);
                        if (isset($content['errors']) && !empty($content['errors']) && is_array($content['errors'])) {
                            // Keep track of previous errors
                            $previous = $content['errors'];
                            $message = 'The error comes from another API, see the next error for details.';
                        }
                    }
                    return \LinckoJson::error($exception->getCode(), $message, [
                        'traces' => $traces,
                    ], $previous);
                } else {
                    return \LinckoJson::error($exception->getCode(), 'An error has occurred while processing your request, the support team has been notified of the problem.');
                }
            });

            // Catch Illuminate validation errors and return into a json format with every warning included
            $this->app->make(ExceptionHandler::class)->renderer(function (IlluminateValidationException $exception, $request) {
                return \LinckoJson::error(422, $exception->getMessage(), $exception->errors());
            });

            // Catch CloudCreativity validation errors and return into a json format with every warning included
            $this->app->make(ExceptionHandler::class)->renderer(function (ValidationException $exception, $request) {
                // Get Generic error message
                $error_message = $exception->getErrors();
                // If the validator exists, we get a more accurate error message
                if ($validator = $exception->getValidator()) {
                    // Get error details
                    $error_message = $validator->getMessageBag()->getMessages();
                // If this is a list of erros
                } elseif ($error_message instanceof ErrorCollection) {
                    // We loop all errors
                    foreach ($error_message as $error) {
                        // Get number of error instances
                        $count = count($error_message);
                        if ($count == 1) {
                            // If only one instance
                            $error_message = $error->getDetail();
                        } elseif ($count > 1) {
                            // If this is a list of errors, we save messages into an array
                            if (!is_array($error_message)) {
                                $error_message = [];
                            }
                            $error_message[] = $error->getDetail();
                        }
                    }
                }
                return \LinckoJson::error($exception->getHttpCode(), $exception->getMessage(), $error_message);
            });
        }
    }

    /**
     * Load package translations.
     *
     * @return void
     */
    protected function loadTranslations()
    {
        $this->loadTranslationsFrom(__DIR__.'/../../resources/lang', 'lincko_json');
    }
}
