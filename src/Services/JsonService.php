<?php

namespace Brunoocto\Json\Services;

use Illuminate\Http\Response;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Brunoocto\Json\Contracts\JsonInterface;

/**
 * Format the response according to API JSON standards
 * https://jsonapi.org/format/
 * https://laravel-news.com/json-api-introduction
 * https://github.com/neomerx/json-api
 *
 */
class JsonService implements JsonInterface
{

    /**
     * List of error codes with HTTP Codes (https://en.wikipedia.org/wiki/List_of_HTTP_status_codes)
     * and human-readable format description
     * Use KEY above 1000 because below are reserved to cover HTTP codes.
     *
     * @var array
     */
    protected const ERROR_CODES = [
        // Global
        1000 => [400, 'Error'],

        // Authentication
        1001 => [401, 'Authorization Required'],

        // Verimatrix
        2000 => [400, 'Verimatrix API error'],
    ];

    /**
     * Enable to send immediatly the response and let Laravel close the process in background
     *
     * @var boolean
     */
    protected $immediate = true;

    /**
     * Force to wait laravel to use its orignal output behavior
     * It's mostly useful for testing mode that do not need to display the response but to assert it only
     *
     * @var boolean
     */
    protected $ignore_immediate = false;

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct()
    {
        if (env('APP_ENV') == 'testing' && !env('LINCKO_JSON_FORCE_IMMEDIATE')) {
            // Do not force the response output in Testing mode to avoid populate the console logs
            $this->ignore_immediate = true;
            $this->immediate = false;
        }
    }

    /**
     * Force to wait that Laravel finish the process to send the output
     * This is the classic behavior of Laravel
     *
     * @return $this
     */
    public function wait()
    {
        $this->immediate = false;
        return $this;
    }

    /**
     * Force to immediatly send the out to the Client and let Laravel finish the process in background
     * Be aware that this may bypass some middlewares that can setup additional feedback information like error message, cookies, and so.
     *
     * @return $this
     */
    public function immediate()
    {
        if (!$this->ignore_immediate) {
            $this->immediate = true;
        }
        return $this;
    }

    /**
     * Convert a Model or a Collection of Model into JsonApi Format.
     * It does generate standard files if it does not exist yet.
     *
     * @param Model|Collection $data Model or Collection of the data
     * @return Object
     */
    protected function convertJsonApi($data)
    {
        // If a single Model
        if ($data instanceof Model) {
            // Get all attributes
            $attributes = $data->toArray();
            // 'type' and 'id' are reserved keys
            unset($attributes['type']);
            unset($attributes['id']);
            // Return a JsonAPI format
            return [
                'type' => $data->getTable(),
                'id' => $data->getRouteKey(),
                'attributes' => $attributes,
            ];
        // If a collection of Model
        } elseif ($data instanceof Collection) {
            // initialize an array to return the collection
            $collection = [];
            foreach ($data as $model) {
                // Get all attributes
                $attributes = $model->toArray();
                // 'type' and 'id' are reserved keys
                unset($attributes['type']);
                unset($attributes['id']);
                // Return a JsonAPI format
                $collection[] = [
                    'type' => $model->getTable(),
                    'id' => $model->getRouteKey(),
                    'attributes' => $attributes,
                ];
            }
            return $collection;
        }

        // return the data not formatted
        return $data;
    }

    /**
     * Send a successul response
     *
     * @param object $data The data send, usually a Database model instance
     * @param int $status HTTP Code
     * @param string $message A human-readable message to inform the client (it will attached to 'meta')
     * @param mixed $meta Any additionnal information we want to send to the client
     * @return Illuminate\Http\JsonResponse
     */
    public function send($data = null, $status = 200, $message = '', $meta = null)
    {
        // If we send only a string, we consider that it is a message, not a data.
        if (empty($message) && is_string($data)) {
            $message = $data;
            $data = null;
        }

        // If the $data is not an object or an array, we convert it as an Array first, the first key will be 0 then.
        if (!is_object($data) && !is_array($data)) {
            $data = (array)$data;
        }

        // Convert a Model to a JsonApi standard format
        $data = $this->convertJsonApi($data);

        // Build the data response
        $json = [
            // The document's "primary data". We make sure it returns an object
            'data' => $data,
        ];

        // We build the meta data information
        if (!is_null($meta) || !empty($message)) {
            // We make sure to return an object
            $json_meta = new \stdClass();
            if (is_string($meta) || is_numeric($meta)) {
                $json_meta->data = $meta;
            } else {
                $json_meta->data = (object)$meta;
            }
            $json_meta->message = (string)$message;
            // A meta object that contains non-standard meta-information.
            $json['meta'] = $json_meta;
        }

        $response = response()
            ->json(
                $json,
                // HTTP Code
                (int)$status
            )
            ->header('Content-Type', 'application/vnd.api+json');

        // Send immediate response to the Client
        if ($this->immediate) {
            $response->send();
            if (env('APP_ENV') != 'testing') {
                exit(0);
            }
        }

        return $response;
    }


    /**
     * Send an error response
     *
     * @param int $code The code corresponding to the error, or a predefined HTTP code
     * @param int $status HTTP Code
     * @param string $message A human-readable message to inform the client (it will attached to 'meta')
     * @param mixed $meta Any additionnal information we want to send to the client
     * @param array $previous Errors array coming from another API catched by Guzzle
     * @return Illuminate\Http\JsonResponse
     */
    public function error($code = 1000, $message = '', $meta = null, $previous = null)
    {
        // Get error information.
        $error = $this->errorMessage($code);
        $status = $error[0];
        $title = $error[1];

        // We build error information
        $json = [
            // A unique ID to identity the error log
            'id' => uniqid(),
            // The HTTP status code applicable to this problem, expressed as a string value.
            'status' => (string)$status,
            // An application-specific error code, expressed as a string value.
            'code' => $code,
            // A short, human-readable summary of the problem, it's simply the convertion of the code.
            'title' => $title,
        ];

        // Initialization
        $json_meta = false;

        // Trace error in dev or debug mode
        if (app()->isLocal() || env('APP_DEBUG') || !is_null($meta)) {
            // We make sure to return an object
            if (!is_object($json_meta)) {
                $json_meta = new \stdClass();
            }
            if (is_string($meta) || is_numeric($meta)) {
                $json_meta->data = $meta;
            } else {
                $json_meta->data = (object)$meta;
            }
            // Inform about the origin of the error in case of micro-services cascade
            $json_meta->app = env('APP_NAME') ?? null;
            $json_meta->server = request()->server('SERVER_ADDR').':'.request()->server('SERVER_PORT'). '['.request()->server('HOSTNAME').']';
            $json_meta->client = request()->server('REMOTE_ADDR').':'.request()->server('REMOTE_PORT');
        }

        // We build the meta data information
        if (!empty($message)) {
            // We make sure to return an object
            if (!is_object($json_meta)) {
                $json_meta = new \stdClass();
            }
            $json_meta->message = (string)$message;
        }

        if ($json_meta !== false) {
            // A meta object that contains non-standard meta-information.
            $json['meta'] = $json_meta;
        }

        // Set errors array
        $errors =  [(object)$json];
        
        // If any previous errors, we concate them to the error message
        if (!empty($previous) && is_array($previous)) {
            $errors = array_merge($errors, $previous);
        }

        $response = response()
            ->json(
                [
                    // An array of error objects
                    'errors' => $errors,
                ],
                // HTTP Code
                (int)$status
            )
            ->header('Content-Type', 'application/vnd.api+json');

        // Send immediate response to the Client
        if ($this->immediate) {
            $response->send();
            if (env('APP_ENV') != 'testing') {
                exit(0);
            }
        }

        return $response;
    }

    /**
     * Send an error response
     *
     * @param int $code The code corresponding to the error
     * @param int|string $status HTTP Code
     * @return Array An array that returns the HTTP code error with an error message
     */
    protected function errorMessage($code)
    {
        if (array_key_exists($code, self::ERROR_CODES)) {
            $info = self::ERROR_CODES[$code];
        } elseif (array_key_exists($code, Response::$statusTexts)) {
            $info = [$code, Response::$statusTexts[$code]];
        } else {
            $info = self::ERROR_CODES[1000];
        }
        return $info;
    }
}
