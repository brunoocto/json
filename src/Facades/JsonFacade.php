<?php

namespace Brunoocto\Json\Facades;

use Illuminate\Support\Facades\Facade;

class JsonFacade extends Facade
{
    /**
    * Get the registered name of the component.
    *
    * @return string
    */
    protected static function getFacadeAccessor()
    {
        return 'lincko_json';
    }
}
