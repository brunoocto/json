<?php

namespace Brunoocto\Json\Contracts;

interface JsonInterface
{
    /**
     * Send a successul response
     *
     * @param object $data The data send, usually a Database model instance
     * @param int $status HTTP Code
     * @param string $message A human-readable message to inform the client (it will attached to 'meta')
     * @param mixed $meta Any additionnal information we want to send to the client
     * @return Illuminate\Http\JsonResponse
     */
    public function send($data = null, $status = 200, $message = '', $meta = null);

    /**
     * Send an error response
     *
     * @param int $code The code corresponding to the error
     * @param int $status HTTP Code
     * @param string $message A human-readable message to inform the client (it will attached to 'meta')
     * @param mixed $meta Any additionnal information we want to send to the client
     * @return Illuminate\Http\JsonResponse
     */
    public function error($code = 1000, $message = '', $meta = null);
}
